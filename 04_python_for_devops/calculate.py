def calculate(operand1, operand2, operator):
    if operator == '+':
        return (operand1 + operand2)
    elif operator == '-':
        return (operand1 - operand2)
    elif operator == '*':
        return (operand1 * operand2)
    elif operator == '/':
        return (operand1 / operand2)
    else:
        print ("You didn't enter a valid input.")

calculate(2, 4, '*')
