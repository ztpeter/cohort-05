#!/usr/bin/env python3
import sys

def calculate(x, y, op):
	if op == '+':
		return x + y
	elif op == '-':
		return x - y

args = sys.argv

result = calculate(int(args[1]), int(args[2]), args[3])

print("Result is:", result)
